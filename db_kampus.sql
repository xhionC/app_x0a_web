-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2020 at 09:43 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kampus`
--

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `NIM` int(11) NOT NULL,
  `ID_PRODI` int(11) DEFAULT NULL,
  `NAMA` varchar(30) DEFAULT NULL,
  `TGL` date DEFAULT NULL,
  `ALMT` varchar(30) DEFAULT NULL,
  `JK` varchar(30) DEFAULT NULL,
  `PHOTO` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`NIM`, `ID_PRODI`, `NAMA`, `TGL`, `ALMT`, `JK`, `PHOTO`) VALUES
(1931733111, 1, 'Muhaidi', '2003-03-12', 'Kediri', 'Laki-Laki', 'muhaidi.jpg'),
(1931733112, 2, 'Joko', '1993-07-28', 'Blitar', 'Laki-Laki', 'joko.jpg'),
(1931733113, 3, 'Sarni', '1994-03-17', 'Nganjuk', 'Perempuan', 'sarni.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `ID_PRODI` int(11) NOT NULL,
  `NAMA_PRODI` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`ID_PRODI`, `NAMA_PRODI`) VALUES
(1, 'Manajemen Informatika'),
(2, 'Teknik Mesin'),
(3, 'Akuntansi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`NIM`),
  ADD KEY `FK_GET_ID_PRODI` (`ID_PRODI`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`ID_PRODI`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `FK_GET_ID_PRODI` FOREIGN KEY (`ID_PRODI`) REFERENCES `prodi` (`ID_PRODI`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
